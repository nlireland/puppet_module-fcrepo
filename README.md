# README #

This is a Fedora Commons repository used to manage and access digital content in the form of digital objects

*Note: This repository was originally forked from:* https://github.com/avalonmediasystem/puppet-fcrepo


### Set up ###

Add the module reference to the Puppetfile of Puppet-Master in order to use this module 
```
mod 'puppet-fcrepo',
  :git => 'https://bitbucket.org/nlireland/puppet_module-fcrepo'
```