# Copyright 2011-2013, The Trustees of Indiana University and Northwestern
#   University.  Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
#   under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#   CONDITIONS OF ANY KIND, either express or implied. See the License for the
#   specific language governing permissions and limitations under the License.
# ---  END LICENSE_HEADER BLOCK  ---

class fcrepo (
  $fedora_base          = $fcrepo::params::fedora_base,
  $fedora_home          = $fcrepo::params::fedora_home,
  $user                 = $fcrepo::params::user,
  $version              = $fcrepo::params::version,
  $fedora_admin_pass    = $fcrepo::params::fedora_admin_pass,
  $group                = $fcrepo::params::group,
  $java_version         = $fcrepo::params::java_version,
  $tomcat_http_port     = $fcrepo::params::tomcat_http_port,
  $tomcat_https_port    = $fcrepo::params::tomcat_https_port,
  $tomcat_shutdown_port = $fcrepo::params::tomcat_shutdown_port,
  $fedora_context       = $fcrepo::params::fedora_context,
  $messaging_uri        = $fcrepo::params::messaging_uri,
  $ri_enabled           = $fcrepo::params::ri_enabled,
  $tomcat_home          = $fcrepo::params::tomcat_home,
  $server_host          = $fcrepo::params::server_host,
  $fedora_db_user       = $fcrepo::params::fedora_db_user,
  $fedora_db_pass       = $fcrepo::params::fedora_db_pass,
  $fedora_db_host       = $fcrepo::params::fedora_db_host,
  $fedora_database      = $fcrepo::params::fedora_database,
  $fedora_data_dir      = $fcrepo::params::fedora_data_dir,
  $fedora_object_store  = $fcrepo::params::fedora_object_store,
  $fedora_datastream_store = $fcrepo::params::fedora_datastream_store,
  $default_namespace = $fcrepo::params::default_namespace,

  ) inherits fcrepo::params {
  include fcrepo::config

  if defined(jdk) {
    include jdk
  }

  if $version == 'latest' {
    $download_url = "http://sourceforge.net/projects/fedora-commons/files/latest/download"
  } else {
    $download_url = "http://downloads.sourceforge.net/fedora-commons/fcrepo-installer-$version.jar"
  }

  File {
    selinux_ignore_defaults => true
  }

  staging::file { 'fcrepo-installer.jar':
    source  => $download_url,
    timeout => 1200,
    subdir  => 'fcrepo'
  }

  file { "$fedora_home":
    ensure => directory,
    owner  => $user,
    group  => $group,
    mode   => '0775',
    require => Class['fcrepo::config']
  }

  notify { 'fedora-install':
    message     => "Checking if installed or installing fedora $version as $user",
    require     => Class['fcrepo::config']
  }->
  exec { 'install-fedora':
    command     => "/usr/bin/java -jar ${staging::path}/fcrepo/fcrepo-installer.jar ${fcrepo::config::propfile}",
    creates     => "$fedora_home/server",
    environment => ["FEDORA_HOME=$fedora_home", "CATALINA_HOME=$tomcat_home"],
    timeout     => 1800,
    user        => $user,
    group       => $group,
    path        => ['/bin', '/usr', '/usr/bin'],
    require     => [Staging::File['fcrepo-installer.jar']],
    notify      => [File["$fedora_home/server/status"], Service["tomcat"]]
    #Concat::Fragment['fedora-tomcat-config']]
  }
  exec { 'remove-installer.properties':
    command => "rm ${fcrepo::config::propfile}",
    path => '/usr/bin',
    require => Exec['install-fedora'],
  }

  file { ["$fedora_home/server/logs", "$fedora_home/server/fedora-internal-use", "$fedora_home/server/management/upload", "$fedora_home/server/work"]:
    ensure  => directory,
    owner   => $user,
    group   => $group,
    mode    => '0775',
    require => Exec['install-fedora']
  }

  file { "$fedora_home/server/status":
    ensure  => present,
    owner   => $user,
    group   => $group,
    mode    => '0664',
    recurse => true,
    require => Exec['install-fedora']
  }

  file { ["$fedora_data_dir", "$fedora_object_store", "$fedora_datastream_store"]:
    ensure  => directory,
    owner   => $user,
    group   => $group,
    mode    => '0775',
    require => Class['fcrepo::config'],
  }

  file { 'akubra-llstore.xml':
    ensure  => present,
    owner   => $user,
    group   => $group,
    path    => "$fedora_home/server/config/spring/akubra-llstore.xml",
    content => template('fcrepo/akubra-llstore.erb'),
    require => Exec['install-fedora'],
    notify  => [File["$fedora_home/server/status"],Service["tomcat"]],
  }

  augeas{'jp2_extension':
    lens    => "Xml.lns",
    incl => "$fedora_home/server/config/mime-to-extensions.xml",
    changes => ["set mappings/mime-mapping[mime-type/#text='image/jp2']/mime-type/#text image/jp2",
                "set mappings/mime-mapping[mime-type/#text='image/jp2']/extension/#text jp2"],
    require => Exec['install-fedora']
  }

  augeas{'dng_extension':
    lens    => "Xml.lns",
    incl => "$fedora_home/server/config/mime-to-extensions.xml",
    changes => ["set mappings/mime-mapping[mime-type/#text='image/x-adobe-dng']/mime-type/#text image/x-adobe-dng",
                "set mappings/mime-mapping[mime-type/#text='image/x-adobe-dng']/extension/#text dng"],
    require => Exec['install-fedora']
  }

  augeas{'default_namespace':
    lens    => "Xml.lns",
    incl => "$fedora_home/server/config/fedora.fcfg",
    changes => ["set //param[#attribute/name='pidNamespace']/#attribute/value $default_namespace"],
    require => Exec['install-fedora']
  }
}
