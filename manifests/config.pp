# Copyright 2011-2013, The Trustees of Indiana University and Northwestern
#   University.  Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
#   under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#   CONDITIONS OF ANY KIND, either express or implied. See the License for the
#   specific language governing permissions and limitations under the License.
# ---  END LICENSE_HEADER BLOCK  ---

class fcrepo::config inherits fcrepo {

	include staging

  file { "${staging::path}/fcrepo":
    ensure => directory
  }

  if $messaging_uri == '' {
    $messaging_enabled = 'false'
  } else {
    $messaging_enabled = 'true'
  }

  if $tomcat_https_port == '' {
    $ssl_available = 'false'
  } else {
    $ssl_available = 'true'
  }

	$propfile = "${staging::path}/fcrepo/installer.properties"
  concat { $propfile:
    require => File["${staging::path}/fcrepo"]
  }

  if $tomcat_home == '' {
	  $install_tomcat = 'true'
  } else {
  	$install_tomcat = 'false'
  	concat::fragment { 'fedora-tomcat-properties':
  		content => template('fcrepo/fedora_tomcat_properties.erb'),
  		target => $propfile
  	}
  }

  concat::fragment { 'fedora-defaults':
    content => template('fcrepo/fedora_installer_properties.erb'),
    target  => $propfile
  }

}
