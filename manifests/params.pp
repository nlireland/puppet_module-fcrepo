# Copyright 2011-2013, The Trustees of Indiana University and Northwestern
#   University.  Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
#   under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#   CONDITIONS OF ANY KIND, either express or implied. See the License for the
#   specific language governing permissions and limitations under the License.
# ---  END LICENSE_HEADER BLOCK  ---

class fcrepo::params {
  $fedora_base          = '/usr/local'
  $fedora_home          = "$fedora_base/fedora"
  $user                 = 'root'
  $version              = '3.6.2' #using 3.6.2 as 3.7.1 doesn't install properly with mysql, 3.8.1+ should work fine too
  $fedora_admin_pass    = 'fedoraAdmin'
  $group                = 'tomcat'
  $java_version         = '1.7.0'
  $tomcat_http_port     = '8080'
  $tomcat_https_port    = '8443'
  $tomcat_shutdown_port = '8005'
  $fedora_context       = 'fedora'
  $messaging_uri        = ''
  $ri_enabled           = 'true'
  $tomcat_home          = '/usr/share/tomcat'
  $server_host          = $::fqdn
  $fedora_db_user       = 'fedoraAdmin'
  $fedora_db_pass       = 'fedoraAdmin'
  $fedora_db_host       = 'localhost'
  $fedora_database      = 'fedora3'
  $fedora_data_dir      = "$fedora_home/data"
  $fedora_object_store  = "$fedora_data_dir/objectStore"
  $fedora_datastream_store = "$fedora_data_dir/datastreamStore"
  $default_namespace = "iedunl"
}
